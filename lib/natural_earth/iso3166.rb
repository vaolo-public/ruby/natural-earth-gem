# frozen_string_literal: true

module NaturalEarth
  module ISO3166 # :nodoc:
    # Normalize an ISO-3166-1 code (alpha-2, alpha-3 or numeric) or ISO-3166-2 code to ISO-3166 alpha-2
    def as_iso3166_1(iso3166)
      return numeric_to_alpha2(iso3166) if numeric? iso3166

      normalized = extract(reformat(iso3166))
      raise ArgumentError if normalized.length > 3 || normalized.length < 2

      return alpha3_to_alpha2(normalized) if normalized.length == 3

      normalized
    end

    # Normalize an ISO-3166-2 code
    def as_iso3166_2(iso3166)
      normalized = reformat(iso3166)
      raise ArgumentError unless normalized.match?(/\A[A-Z]{2}-[A-Z0-9]+\z/)

      normalized
    end

    protected

    def reformat(code)
      code.to_s.upcase.tr('_', '-')
    end

    def extract(code)
      return code.split('-').first if code.include?('-')

      code
    end

    def numeric?(code)
      code.is_a?(Numeric) || code.to_s.to_i.to_s == code
    end

    def alpha3_to_alpha2(code)
      as_string = code.to_s
      raise ArgumentError unless alpha3_to_alpha2_map.key? as_string

      alpha3_to_alpha2_map[as_string]
    end

    def numeric_to_alpha2(code)
      as_int = code.to_i
      raise ArgumentError unless numeric_to_alpha2_map.key? as_int

      numeric_to_alpha2_map[as_int]
    end

    def alpha3_to_alpha2_map
      @alpha3_to_alpha2_map ||= COUNTRIES.each_with_object({}) do |(_, c), n|
        n[c.dig('iso-3166-1', 'alpha-3')] = c.dig('iso-3166-1', 'alpha-2')
      end.freeze
    end

    def numeric_to_alpha2_map
      @numeric_to_alpha2_map ||= COUNTRIES.each_with_object({}) do |(_, c), n|
        n[c.dig('iso-3166-1', 'numeric')] = c.dig('iso-3166-1', 'alpha-2')
      end.freeze
    end
  end
end
