# frozen_string_literal: true

require 'multi_json'
require 'rgeo/geo_json'

module NaturalEarth
  module Geometries # :nodoc:
    COUNTRY_PATH = 'geometries/%<iso3166_1>s/%<iso3166_1>s.json'
    SUBDIVISION_PATH = 'geometries/%<iso3166_1>s/%<iso3166_2>s.json'

    class << self
      include ISO3166

      def country(iso3166)
        raise KeyError unless Countries.supported?(iso3166)

        load(path(COUNTRY_PATH, iso3166_1: as_iso3166_1(iso3166)))
      end

      def subdivision(iso3166)
        raise KeyError unless Subdivisions.supported?(iso3166)

        load(path(SUBDIVISION_PATH, iso3166_1: as_iso3166_1(iso3166), iso3166_2: as_iso3166_2(iso3166)))
      end

      protected

      def path(pattern, args = {})
        File.join(NaturalEarth::RESOURCES_DIR, format(pattern, **args))
      end

      def load(file_path)
        raise ArgumentError unless File.exist?(file_path)
        raise ArgumentError unless File.readable?(file_path)

        RGeo::GeoJSON.decode(MultiJson.load(File.read(file_path)))
      end
    end
  end
end
