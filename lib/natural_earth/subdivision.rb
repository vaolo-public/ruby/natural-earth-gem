# frozen_string_literal: true

module NaturalEarth
  # A NaturalEarth subdivision
  class Subdivision
    attr_reader :data

    def initialize(data)
      @data = data
    end

    def iso3166(_format: nil)
      data['iso-3166-2']
    end

    def name
      data['name']
    end

    def country
      Countries[data['country']]
    end

    def geometry
      Geometries.subdivision(iso3166)
    end
  end
end
