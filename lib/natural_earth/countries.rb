# frozen_string_literal: true

module NaturalEarth
  # Give access to the list of supported countries
  module Countries
    class << self
      include ISO3166
      alias normalize as_iso3166_1

      # Return the list of supported countries as iso-3166-1 alpha-2 codes
      def supported
        COUNTRIES.keys
      end

      # Check if a country is supported
      def supported?(iso3166_1)
        COUNTRIES.key?(normalize(iso3166_1))
      end

      # Fetch information about a country, as a NaturalEarth::Country instance
      def fetch(iso3166_1)
        raise KeyError unless supported?(iso3166_1)

        NaturalEarth::Country.new(COUNTRIES[normalize(iso3166_1)])
      end

      alias keys supported
      alias key? supported?
      alias [] fetch
    end
  end
end
