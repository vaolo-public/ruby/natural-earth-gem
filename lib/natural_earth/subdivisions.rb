# frozen_string_literal: true

module NaturalEarth
  # Give access to the list of supported subdivisions
  module Subdivisions
    class << self
      include ISO3166
      alias normalize as_iso3166_2

      # Return the list of supported subdivisions, as iso-3166-2 codes
      def supported
        SUBDIVISIONS.keys
      end

      # Check if a subdivision is supported
      def supported?(iso3166)
        SUBDIVISIONS.key?(normalize(iso3166))
      end

      # Fetch information about a subdivision, as a NaturalEarth::Subdivision instance
      def fetch(iso3166)
        raise KeyError unless supported?(iso3166)

        NaturalEarth::Subdivision.new(SUBDIVISIONS[normalize(iso3166)])
      end

      alias keys supported
      alias key? supported?
      alias [] fetch
    end
  end
end
