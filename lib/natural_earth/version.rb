# frozen_string_literal: true

module NaturalEarth
  VERSION = '4.1.1'
end
