# frozen_string_literal: true

module NaturalEarth
  # A NaturalEarth country
  class Country
    attr_reader :data

    def initialize(data)
      @data = data
    end

    def iso3166(format: 'alpha-2')
      data.dig('iso-3166-1', format)
    end

    def name
      data['name']
    end

    def continent
      data['continent']
    end

    def region
      data['region']
    end

    def subdivisions
      data['subdivisions'].each_with_object({}) { |iso3166, subs| subs[iso3166] = Subdivisions[iso3166] }
    end

    def geometry
      Geometries.country(iso3166)
    end
  end
end
