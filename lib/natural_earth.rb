# frozen_string_literal: true

require 'multi_json'
require_relative 'natural_earth/version'
require_relative 'natural_earth/iso3166'
require_relative 'natural_earth/countries'
require_relative 'natural_earth/subdivisions'
require_relative 'natural_earth/geometries'
require_relative 'natural_earth/country'
require_relative 'natural_earth/subdivision'

module NaturalEarth # :nodoc:
  class << self
    protected

    def load(resource_name)
      path = File.join(NaturalEarth::RESOURCES_DIR, "#{resource_name}.json")
      MultiJson.load(File.read(path)) if File.exist?(path) && File.readable?(path)
    end
  end

  RESOURCES_DIR = File.join(File.dirname(File.dirname(File.expand_path(__FILE__))), 'resources')

  COUNTRIES     = load(:countries).freeze
  SUBDIVISIONS  = load(:subdivisions).freeze
end
