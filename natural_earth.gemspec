# frozen_string_literal: true

require_relative 'lib/natural_earth/version'

Gem::Specification.new do |s|
  s.name        = 'natural_earth'
  s.version     = NaturalEarth::VERSION
  s.licenses    = ['CC0-1.0']
  s.authors     = ['Vaolo']
  s.email       = ['techservices@vaolo.com']

  s.summary     = "Ruby wrapper around NaturalEarth project's data"
  s.description = 'Ruby wrapper around data from Natural Earth, free vector and raster map data @ naturalearthdata.com.'
  s.homepage    = 'https://gitlab.com/vaolo-public/ruby/natural-earth-gem'

  s.metadata['rubygems_mfa_required'] = 'true'
  s.metadata['allowed_push_host'] = 'https://rubygems.org'
  s.metadata['homepage_uri'] = s.homepage
  s.metadata['source_code_uri'] = s.homepage
  s.metadata['changelog_uri'] = "#{s.homepage}/-/blob/master/CHANGELOG.md"

  s.require_paths = ['lib']
  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  s.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end

  s.required_ruby_version = '>= 2.7.0'

  s.add_dependency 'rgeo-geojson', '~> 2.1'
end
