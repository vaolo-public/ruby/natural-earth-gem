# frozen_string_literal: true

RSpec.describe NaturalEarth::Subdivisions do
  subject(:subdivisions) { described_class }

  describe '.supported' do
    it 'returns the list of supported subdivisions' do
      expect(subdivisions.supported).to be_a(Array)
    end
  end

  describe '.supported?' do
    it 'returns true for a supported ISO-3166-2' do
      expect(subdivisions).to be_supported('CA-QC')
    end

    it 'returns false for a unsupported ISO-3166-2' do
      expect(subdivisions).not_to be_supported('ZZ-00')
    end
  end

  describe '.fetch' do
    it 'returns a NaturalEarth::Subdivision' do
      expect(subdivisions.fetch('CA-QC')).to be_a(NaturalEarth::Subdivision)
    end

    it 'raises a KeyError on unsupported request' do
      expect { subdivisions.fetch('ZZ-00') }.to raise_error(KeyError)
    end
  end
end
