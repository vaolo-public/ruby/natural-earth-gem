# frozen_string_literal: true

RSpec.describe NaturalEarth::Subdivision do
  subject(:subdivision) { NaturalEarth::Subdivisions.fetch('CA-QC') }

  describe '#iso3166' do
    it 'returns the iso-3166-1 code of the country' do
      expect(subdivision.iso3166).to eq('CA-QC')
    end
  end

  describe '#name' do
    it 'returns the name of the subdivision' do
      expect(subdivision.name).to eq('Québec')
    end
  end

  describe '#country' do
    it 'returns a country' do
      expect(subdivision.country).to be_a(NaturalEarth::Country)
    end

    it 'returns the parent country of the subdivision' do
      expect(subdivision.country.iso3166).to eq('CA')
    end
  end

  describe '#geometry' do
    it 'returns a RGeo::Feature' do
      expect(subdivision.geometry).to be_a(RGeo::Feature::Instance)
    end
  end
end
