# frozen_string_literal: true

require 'twitter_cldr'

RSpec.describe NaturalEarth::Countries do
  subject(:countries) { described_class }

  describe '.supported' do
    it 'returns the list of supported countries' do
      expect(countries.supported).to be_a(Array)
    end

    it 'supports all countries with an ISO-3166-1 code' do
      expect(countries.supported).to include(*iso3166_country_codes)
    end
  end

  describe '.supported?' do
    it 'returns true for a supported ISO-3166' do
      expect(countries).to be_supported('CA')
    end

    it 'returns false for a supported ISO-3166' do
      expect(countries).not_to be_supported('ZZ')
    end
  end

  describe '.fetch' do
    it 'returns a NaturalEarth::Country' do
      expect(countries.fetch('CA')).to be_a(NaturalEarth::Country)
    end

    it 'raises a KeyError on unsupported request' do
      expect { countries.fetch('ZZ') }.to raise_error(KeyError)
    end
  end

  protected

  def iso3166_country_codes
    country_codes = TwitterCldr::Shared::Territories.all.keys.collect(&:to_s).collect(&:upcase)

    # Remove exceptional reservations
    # See <https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Exceptional_reservations>
    country_codes -= %w[AC CP CQ DG EA EU EZ FX IC SU TA UK UN]
    # Remove some specific uses
    # See <https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#User-assigned_code_elements>
    country_codes -= %w[QO XA XB XK ZZ]
    # Remove Taiwan as now part of China
    country_codes -= %w[TW]
    country_codes
  end
end
