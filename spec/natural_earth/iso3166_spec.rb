# frozen_string_literal: true

RSpec.describe NaturalEarth::ISO3166 do
  subject(:iso) { klass.new }

  let(:klass) { Class.new { include NaturalEarth::ISO3166 } }

  describe '.as_iso3166_1' do
    it 'accepts ISO-3166-1 alpha-2 as an uppercase string' do
      expect(iso.as_iso3166_1('CA')).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-2 as a downcase string' do
      expect(iso.as_iso3166_1('ca')).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-2 as an uppercase symbol' do
      expect(iso.as_iso3166_1(:CA)).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-2 as an downcase symbol' do
      expect(iso.as_iso3166_1(:ca)).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-3 as an uppercase string' do
      expect(iso.as_iso3166_1('CAN')).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-3 as a downcase string' do
      expect(iso.as_iso3166_1('can')).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-3 as an uppercase symbol' do
      expect(iso.as_iso3166_1(:CAN)).to eq('CA')
    end

    it 'accepts ISO-3166-1 alpha-3 as a downcase symbol' do
      expect(iso.as_iso3166_1(:can)).to eq('CA')
    end

    it 'accepts ISO-3166-1 numeric as an integer' do
      expect(iso.as_iso3166_1(124)).to eq('CA')
    end

    it 'accepts ISO-3166-1 numeric as a string' do
      expect(iso.as_iso3166_1('124')).to eq('CA')
    end

    it 'accepts ISO-3166-2 as an uppercase string' do
      expect(iso.as_iso3166_1('CA-QC')).to eq('CA')
    end

    it 'accepts ISO-3166-2 as a downcase string' do
      expect(iso.as_iso3166_1('ca-qc')).to eq('CA')
    end

    it 'accepts ISO-3166-2 as an uppercase symbol' do
      expect(iso.as_iso3166_1(:'CA-QC')).to eq('CA')
    end

    it 'accepts ISO-3166-2 as a downcase symbol' do
      expect(iso.as_iso3166_1(:'ca-qc')).to eq('CA')
    end

    it 'raises an ArgumentError when code is too long' do
      expect { iso.as_iso3166_1('invalid') }.to raise_error(ArgumentError)
    end

    it 'raises an ArgumentError when code is too short' do
      expect { iso.as_iso3166_1('i') }.to raise_error(ArgumentError)
    end
  end

  describe '.as_iso3166_2' do
    it 'accepts ISO-3166-2 as an uppercase string' do
      expect(iso.as_iso3166_2('CA-QC')).to eq('CA-QC')
    end

    it 'accepts ISO-3166-2 as a downcase string' do
      expect(iso.as_iso3166_2('ca-qc')).to eq('CA-QC')
    end

    it 'accepts ISO-3166-2 as an uppercase symbol' do
      expect(iso.as_iso3166_2(:'CA-QC')).to eq('CA-QC')
    end

    it 'accepts ISO-3166-2 as a downcase symbol' do
      expect(iso.as_iso3166_2(:'ca-qc')).to eq('CA-QC')
    end

    it 'raises an Argument error when code does not match the format' do
      expect { iso.as_iso3166_2('invalid-qc') }.to raise_error(ArgumentError)
    end
  end
end
