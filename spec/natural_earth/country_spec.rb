# frozen_string_literal: true

RSpec.describe NaturalEarth::Country do
  subject(:country) { NaturalEarth::Countries.fetch('CA') }

  describe '#iso3166' do
    it 'returns the iso-3166-1 code of the country' do
      expect(country.iso3166).to eq('CA')
    end

    {
      'alpha-2' => 'CA',
      'alpha-3' => 'CAN',
      'numeric' => 124
    }.each do |format, expected|
      it "can return the iso-3166-1 code of the country as #{format}" do
        expect(country.iso3166(format: format)).to eq(expected)
      end
    end
  end

  describe '#name' do
    it 'returns the name of the country' do
      expect(country.name).to eq('Canada')
    end
  end

  describe '#continent' do
    it 'returns the continent of the country' do
      expect(country.continent).to eq('North America')
    end
  end

  describe '#region' do
    it 'returns the subregion of the country' do
      expect(country.region).to eq('Northern America')
    end
  end

  describe '#subdivisions' do
    it 'returns a Hash' do
      expect(country.subdivisions).to be_a(Hash)
    end

    it 'returns a Hash of Subdivisions' do
      expect(country.subdivisions.values).to all(be_a(NaturalEarth::Subdivision))
    end
  end

  describe '#geometry' do
    it 'returns a RGeo::Feature' do
      expect(country.geometry).to be_a(RGeo::Feature::Instance)
    end
  end
end
