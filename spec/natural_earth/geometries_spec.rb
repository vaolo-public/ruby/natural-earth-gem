# frozen_string_literal: true

require 'rgeo'

RSpec.describe NaturalEarth::Geometries do
  subject(:geometries) { described_class }

  describe '.country' do
    it 'returns a RGeo::Feature' do
      expect(geometries.country('CA')).to be_a(RGeo::Feature::Instance)
    end

    it 'raises a KeyError on unsupported country code' do
      expect { geometries.country('ZZ') }.to raise_error(KeyError)
    end
  end

  describe '.subdivision' do
    it 'returns a RGeo::Feature' do
      expect(geometries.country('CA-QC')).to be_a(RGeo::Feature::Instance)
    end

    it 'raises a KeyError on unsupported country code' do
      expect { geometries.country('ZZ-00') }.to raise_error(KeyError)
    end
  end
end
