## [Unreleased]

## [4.1.1] - 2023-04-25

### Fixed

- Remove subdivisions without official ISO-3166-2 code (ex: `KI-X01~`)

### Changed

- Upgrade development environment to Ruby 3.2
- Regenerate resources from the latest NaturalEarth release with:
  * Taiwan (`TW`) as a China subdivision
  * Kosovo (`XK`) as an unrecognized sovereign state
  * Renamed subdivisions in some countries to match ISO-3166-2 codes

## [4.1.0] - 2021-08-26

- Initial release
