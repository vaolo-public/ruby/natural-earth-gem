# NaturalEarth

`NaturalEarth` bundles data from the [Natural Earth project](https://www.naturalearthdata.com/) so they can be used in Ruby project.

## Installation

This gem use [RGeo](https://github.com/rgeo/rgeo) internally. Make sure you read [installation instructions for RGeo](https://github.com/rgeo/rgeo).

`NaturalEarth` is distributed as a gem and available [on rubygems.org](https://rubygems.org/gems/natural_earth) so you can add it to your Gemfile or install it manually with:

```shell
gem install natural_earth
```

## Usage

### `NaturalEarth::Countries`

`NaturalEarth::Countries` gives you information about countries.

A country is identified by its [ISO-3166-1 code](https://en.wikipedia.org/wiki/ISO_3166-1). Methods that take an ISO-3166-1 code as argument accept it in any format (alpha-2, alpha-3 or numeric), as a case-insensitive string or symbol (or an integer for numeric codes). An [ISO-3166-2 code](https://en.wikipedia.org/wiki/ISO_3166-2) will be truncated as a 3166-1 code.

An instance of `NaturalEarth::Country` contains data for:

- Its official name, in english. (For country names translations, see other gems like [twitter-cldr](https://github.com/twitter/twitter-cldr-rb) or [carmen](https://github.com/carmen-ruby/carmen))
- Its ISO-3166 codes (alpha-2, alpha-3 and numeric)
- Its location, expressed as a continent and a subregion, as defined by [United Nation geoscheme](https://en.wikipedia.org/wiki/United_Nations_geoscheme)
- Its subdivisions, as a hash of `NaturalEarth::Subdivision`s indexed by their ISO-3166-2 code
- Its geometry, returned as a `RGeo::Feature::Instance`

### `NaturalEarth::Subdivisions`

`NaturalEarth::Subdivisions` gives you information about country subdivisions.

A country subdivision is identified by its [ISO-3166-2 code](https://en.wikipedia.org/wiki/ISO_3166-2). Methods that take an ISO-3166-2 code as argument accept it as a case-insensitive string or symbol.

An instance of `NaturalEarth::Subdivision` contains data for:

- Its official name, in english.
- Its ISO-3166 code
- The country it belongs to, as an instance of `NatiuralEarth::Country`
- Its geometry, returned as a `RGeo::Feature::Instance`

### Interfaces

Both `NaturalEarth::Countries` and `NaturalEarth::Subdivisions` support the same interface:

```ruby
# Get the list of supported regions
NaturalEarth::Countries.supported
# => ['AD', 'AE', 'AF', 'AG', 'AI', 'AL', 'AM', 'AO', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AW', 'AX', 'AZ', …]

# Check if a region is supported
NaturalEarth::Countries.supported?('CA')
# => true

# Fetch a region
NaturalEarth::Countries.fetch('CA')
# => #<NaturalEarth::Country>

NaturalEarth::Countries['CA']
# => #<NaturalEarth::Country>

# Fetch a subdivision
NaturalEarth::Subdivisions.fetch('CA-QC')
# => #<NaturalEarth::Subdivision>

# Get geometry of a region
region = NaturalEarth::Countries.fetch('CA')
region.geometry
# => #<RGeo::Feature::Instance>
```

## A note on data

Cultural geography is hard and changing and data from the Natural Earth project are not perfect. We had to do choices when importing them.

The most important of them is we choose to stick to the ISO-3166 classification as closely as we could. That means some subdivisions have been promoted as countries (e.g., Gibraltar, Bouvet Island), others have been ignored (e.g., subdivisions of Hong-Kong).

For a list of all adjustments we had to do, you can read [`lib/tasks/import.rake`](./lib/tasks/import.rake). Feel free to open an issue or a merge request to discuss these choices and help us to make this gem better.

## Contributing

Bug reports and pull requests are welcome on Gitlab at <https://gitlab.com/vaolo-public/ruby/natural-earth-gem>.

## License

Made with Natural Earth. Free vector and raster map data @ naturalearthdata.com.

`natural_earth` inherits its license from the [Natural Earth project](https://www.naturalearthdata.com/about/terms-of-use/).
